import fr.cnam.foad.nfa035.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.test.SimpleAccessTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


/**
 * Cette classe fournit des cas de test pour les opérations DAO liées à BadgeWallet.
 * Les tests sont axés sur la fonctionnalité d'ajout d'un badge au portefeuille et
 * s'assurent que les images des badges sont sérialisées et stockées correctement.
 *
 * @author Thibault Lombardo
 * @version 1.0
 */

public class DAOTest {

    private static final String RESOURCES_PATH = "C:\\Users\\tlombardo\\exercice-4\\badges-wallet\\src\\test\\resources\\";
    private static final Logger LOG = LogManager.getLogger(DAOTest.class);
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");

    /**
     * Initialise les ressources nécessaires avant chaque test.
     * Notamment, elle s'assure que le fichier de la base de données du portefeuille est propre.
     *
     * @throws IOException si une erreur se produit lors de l'accès au fichier.
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * Teste la fonctionnalité d'ajout d'un badge au portefeuille.
     * Elle vérifie que l'image du badge est correctement sérialisée et stockée.
     */
    @Test
    public void testAddBadge(){
        try {

            File image = new File(RESOURCES_PATH + "petite_image.png");
            BadgeWalletDAO dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet.csv");
            dao.addBadge(image);

            String serializedImage = new String(Files.readAllBytes(walletDatabase.toPath()));
            LOG.info("Le badge-wallet contient à présent cette image sérialisée:\n{}", serializedImage);

            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);

            // astuce pour ignorer les différences de formatage entre outils de sérialisation base64:
            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;

            assertEquals(serializedImage, encodedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }
}
