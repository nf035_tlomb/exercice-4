package fr.cnam.foad.nfa035.dao;
import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageFileFrame;
import java.io.*;


/**
 * DAO pour gérer le porte-badge.
 */
public class BadgeWalletDAO {
    private File walletDatabase;
    private ImageFileFrame imageFileFrame;


    /**
     * Constructeur pour initialiser le DAO avec le chemin d'accès à la base de données du porte-badge.
     *
     * @param filePath Le chemin d'accès au fichier de la base de données du porte-badge.
     */
    public BadgeWalletDAO(String filePath) {
        this.walletDatabase = new File(filePath);
        this.imageFileFrame = new ImageFileFrame(walletDatabase);
    }


    /**
     * Ajoute un badge à la base de données du porte-badge.
     *
     * @param image Le fichier image du badge à ajouter.
     * @throws RuntimeException Si une erreur se produit lors de l'ajout du badge.
     */
    public void addBadge(File image) {
        try (InputStream imageInputStream = new FileInputStream(image);
             OutputStream outputStream = imageFileFrame.getEncodedImageOutput()) {

            byte[] buffer = new byte[1024];
            int bytesRead;

            while ((bytesRead = imageInputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

        } catch (IOException e) {
            throw new RuntimeException("Erreur lors de l'ajout du badge", e);
        }
    }
}
